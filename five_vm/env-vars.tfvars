
  subscription_id= "49d2709f-3abb-47e9-abb6-ce0c26be47f2"
  client_id= "d1fef633-a370-4822-bc5c-0dc2ba41d41c"
  client_secret= "62381b0e-f22b-45fd-8525-4ec06cc32867"
  tenant_id= "edfa0b8b-916d-4bb7-aab9-4db7fb09786a"
  nom_resource= "RG_Formation"
  network_name= "myVnet"
  subnet= "mySubnet"
  publicIP= "myPublicIP1"
  NetworkSecurityGroup= "myNetworkSecurityGroup1"
  NetworkInterface= "myNIC1"
  NicConfiguration= "myNicConfiguration"
  name_virtual_machine= "myVM1"
  security_rule= "SSH"
  security_rule2= "HTTP"
  storage_disk = "myOsDisk1"
  location_world= "eastus"
  environment_name= "Terraform Demo"
  public_ip_address_allocation_name = "static"
  address_space_number =  "10.0.0.0/16"
  ip_addresses = ["10.0.2.5", "10.0.2.6", "10.0.2.7"]


  ## CINQUIEME MACHINE

  nom_resource5= "RG_Formation5"
  location_world5= "West Europe"
  environment_name5= "Terraform Demo5"
  address_space_number5 =  "10.0.0.0/16"
  network_name5= "myVnet5"
  subnet5= "mySubnet5"
  ip_addresses5= "10.0.2.8"
  publicIP5= "myPublicIP5"
  public_ip_address_allocation_name5 = "dynamic"
  NetworkInterface5= "myNIC5"
  NicConfiguration5= "myNicConfiguration5"
  NetworkSecurityGroup5= "myNetworkSecurityGroup5"
  name_virtual_machine5= "myVM5"
  storage_disk5 = "myOsDisk5"